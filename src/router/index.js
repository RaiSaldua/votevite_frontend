import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import Info from '../views/Info.vue'
import MainScreen from '../views/MainScreen.vue'
import Payment from '../views/Payment.vue'
import PreviewStats from '../views/PreviewStats.vue'
import EventScreen from '../views/Events.vue'
import SuccessScreens from '../views/Success.vue'
import IOSsession from '../views/IOSsession.vue';
// import { successUrlJsonChecker } from '../helpers/urlDataChecker.helper';
import SuccessRedirectionScreen from '../views/successResult.vue';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/votingPage',
    name: 'MainScreen',
    component: MainScreen
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  {
    path: '/payment',
    name: 'Payment',
    component: Payment
  },
  {
    path: '/about',
    name: 'Info',
    component: Info
  },
  {
    path: '/statistics',
    name: 'PreviewStats',
    component: PreviewStats
  },
  {
    path: '/',
    name: 'EventScreen',
    component: EventScreen
  },
  {
    path: '/response',
    name: 'successScreen',
    component: SuccessScreens,

    // beforeEnter: async (to, from, next) => {
    //   console.log('route')
    //   if (await successUrlJsonChecker()) {
    //       // next({name: 'general-overview'});
    //       console.log('if', this.$router)
    //   } else {
    //       // next();
    //       console.log('else')
    //   }
    // },
  },
  {
    path: '/res',
    name: 'successRedirectionScreen',
    component: SuccessRedirectionScreen
  },
  {
    path: '/init',
    name: 'IOSsession',
    component: IOSsession
  }
]

const router = new VueRouter({
  // mode: "history",
  base: process.env.BASE_URL,
  routes
})

export default router
