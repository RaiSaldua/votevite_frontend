import  {firebase} from "@firebase/app";
import { FIREBASE_CONFIG } from "./base";
import "@firebase/firestore";
import "@firebase/analytics"
import "@firebase/auth";
import "@firebase/functions";

export const firebaseApp = firebase.initializeApp(FIREBASE_CONFIG);
export const analytics = firebase.analytics();
export const annonSignIn = firebase.auth().signInAnonymously().catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;

  });;
export const anonuser = firebase.auth().onAuthStateChanged((user)=> {

    if (user) {
        var isAnonymous = user.isAnonymous;
        var uid = user.uid;
        return true
    } else {
        return false;
    }
});
export const db = firebaseApp.firestore();
export const firebaseFunctions = firebaseApp.functions();
// export const db