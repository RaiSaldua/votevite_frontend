import { db, annonSignIn, anonuser, firebaseFunctions } from './firebase'
import "@firebase/auth";
import { firebase } from "@firebase/app";

export const getSession = async (sessionID, cb) => {

    await firebase.auth().signInAnonymously()
    const session = db.collection('webSessions').doc(sessionID);
    const doc = session.get().then(snapshot => {
        if (!snapshot.exists) {
            let nodata = snapshot.data()
            return cb(null)
        }

        let data = snapshot.data();
        cb(null, { ...data, webSessionId: snapshot.id })
    });

}

export const postNewSession = async (data, cb) => {

    await firebase.auth().signInAnonymously()
    const res = await db.collection('webSessions').add(data);

    cb(null, res.id)

}

export const getEventID = async (eventID, cb) => {

    await firebase.auth().signInAnonymously()
    const event = db.collection('events').doc(eventID);
    const doc = await event.get().then(snapshot => {
        if (!snapshot.exists) return 0;
        let data = snapshot.data()

        cb(null, data)
    });

}

export const storeSuccessPayment = async (data, cb) => {
    const res = await (await firebaseFunctions.httpsCallable('sendAnswer')(data)).data
    cb(null, res)
}

export const errStoringDonePayment = async (data, cb) => {
    const currentDate = firebase.firestore.Timestamp.fromDate(new Date());
    const res = await db.collection("failedStoringVoteDonePayment").add({ ...data, date: currentDate })
    cb(null, res)
}

export const delSuccessPayment = async (sessionID, cb) => {
    // const res = 'yey'
    const res = await db.collection('webSessions').doc(sessionID).delete();
    cb(null, res)
}