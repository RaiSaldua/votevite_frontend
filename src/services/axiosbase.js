import axios from "axios"

axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'XCSRF-TOKEN';

export const clientId = process.env.VUE_APP_AUTH2_CLIENT_ID;
export const clientSecret = process.env.VUE_APP_AUTH2_CLIENT_SECRET;
export const clientGrant = process.env.VUE_APP_AUTH2_GRANT_TYPE;

export const axiosInstance = axios.create({
    baseURL: process.env.VUE_APP_API_URL,

});
