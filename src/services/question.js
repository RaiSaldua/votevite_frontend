import {db, annonSignIn, anonuser, firebaseFunctions} from './firebase'
import "@firebase/auth";
import  {firebase} from "@firebase/app";

export const getVotes = (eventId, cb) => {
    var query = db.collection("events").doc(eventId.id).collection("votevite_answer_info")
    query.onSnapshot((querySnapshot) => {
        const votes = []
        querySnapshot.forEach(doc => {
            const vote = doc.data()
            votes.push(vote)
        })
        cb(null, votes)
    })
}

export const getEvents = async (cb) => {
    await firebase.auth().signInAnonymously()

    var query = db.collection("events")
    query.onSnapshot((querySnapshot)=> {
        const events = []
        querySnapshot.forEach(doc => {
            const event = doc.data()
            events.push(event)
        })
        cb(null, events)
    })
}

// next = returns boolean
export const postVote = (data, next) => {
    firebaseFunctions
    .httpsCallable("sendAnswer")(data)
    .then((result) => {
        next(result["success"] === true)
    })
}
