import { axiosInstance } from "./axiosbase"
import  {firebase} from "@firebase/app";
import "@firebase/firestore";
import {db} from './firebase'

export function getPayment(webSession){
    const randomDate = Date.now() + Math.floor(Math.random() * 10)
    var myTimestamp = firebase.firestore.Timestamp.fromDate(new Date());
    const data = {transactionReference: randomDate, webSessionId: webSession}
    return axiosInstance.post(`https://us-central1-vote-vite.cloudfunctions.net/payment`, webSession)
}

export function postPayment(data, url){
    const headers = `Access-Control-Allow-Origin: *`
    return axiosInstance.post(`https://us-central1-vote-vite.cloudfunctions.net/payment/redirect`, data, headers)
}

export const postWebSession = async (data) => {
    console.log('data',data)
    await firebase.auth().signInAnonymously()
    if(data.webSessionId){
        const sessionRef = db.collection("webSessions").doc(data.webSessionId);
        const res = sessionRef.set({
            ...data
          }, { merge: true });
          return res
    } else{
        const transactionReference = data.transactionReference.toString();
        db.collection("webSessions").add(data)
    }
}

export function getLocation(){
    const headers = `Access-Control-Allow-Origin: *`
    return axiosInstance.get(`https://geolocation-db.com/jsonp/`, headers)
}