
import { Bar } from 'vue-chartjs'
 
export default {
  extends: Bar,
  props: ['chartdata'],
  data() {
    return {
      options: {
        responsive: true, 
        scales: {
          xAxes: [{
          stacked: true,
          categoryPercentage: 0.5,
          barPercentage: 1
          }],
          yAxes: [{
            stacked: true
          }]
        },
        legend:{
            position: 'bottom'
        },
        gridLines: {
          display: false
        },
        // tooltips:{
        //   borderColor: 'rgba(255, 255, 255, 1)'
        // }
      }
    }
  },
  mounted () {
    // Overwriting base render method with actual data.
    // console.log('CHART', this.chartdata)
    // if(this.chartdata){

      this.renderChart(this.chartdata, this.options)
    // }
  }
}
