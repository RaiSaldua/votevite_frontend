import Vue from 'vue'
import Vuex from 'vuex'

import AxiosPlugin from 'vue-axios-cors';
 
Vue.use(AxiosPlugin)

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
