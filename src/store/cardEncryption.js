import Hmac from "hmac";
import Blake2s from "blake2s";


function createBlake2s () {
    return new Blake2s()
}

const createHmac = Hmac.bind(null, createBlake2s, 64)

var hmac = createHmac('H6znJWqPj9cTRFe9eT62ulurkb_Lrksbw4PFtMSVb74')
const mac = hmac.update('2111122a0', 'utf8').digest('hex')

// console.log('mac', mac)